// BOS Pony Control System v2 - Groom HUD Main Script
// (c) 2023, Claire Morgenthau

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

integer xLow;
integer xHigh;
integer yLow;
integer yHigh;
integer zLow;
integer zHigh;
integer every;

// Adjust only if necessary
integer TICK = 5;

// ----- END OF ADJUSTABLES -----

list _Details = [OBJECT_NAME, OBJECT_POS];
list EMPL = [];

integer _LastSniff;

// ########## FUNCTIONS ##########

integer LSDReadIntDefault(string k, integer def) {
    if ((k = llLinksetDataRead(k)) == "") return def;
    return (integer)k;
}

Sniff() {
    list agents = llGetAgentList(AGENT_LIST_REGION, EMPL);
    integer idx = -(agents != EMPL);

    string ts = (string)llGetUnixTime();
    key k;
    string sel;
    list params;
    string nameUser;
    string nameDisp;
    vector pos;
    string datum;
    do {
        if (llGetAgentSize(k = llList2Key(agents, idx)) != ZERO_VECTOR ) {
            pos = llList2Vector((params = llGetObjectDetails(k, _Details)), 1);
            if (
                pos.x <= xHigh && pos.x >= xLow &&
                pos.y <= yHigh && pos.y >= yLow &&
                pos.z <= zHigh && pos.z >= zLow
            ) {
                nameUser = llList2String(params, 0);
                // Escape double-quotes in display name
                nameDisp = llReplaceSubString(llGetDisplayName(k), "\"", "\\\"", 0);
                sel = "a:" + (string)k;
                if ((datum = llLinksetDataRead(sel)) == "") {
                    // datum = "{\"u\":" + nameUser + ",\"d\":" + nameDisp + ",\"t\":[]}";
                    datum = "u=\"" + nameUser + "\"\nd=\"" + nameDisp + "\"\nt=";
                }
                // datum = llJsonSetValue(datum, ["t", JSON_APPEND], ts);
                datum += "," + ts;
                llLinksetDataWrite(sel, datum);
            }
            params = EMPL;
            llSleep(0.1);
        }
    } while(++idx);

    agents = EMPL;
    llSleep(0.1);
}

TellToucher(string msg) {
    key toucher = (key)llLinksetDataRead("_t");
    if (toucher != NULL_KEY) llRegionSayTo(toucher, 0, msg);
}

// ########## STATES ##########

// WARNING : DO NOT EVER call llLinksetDataReset() in this script !!
//           That is the privilege of main.lsl ONLY !!

default {
    on_rez(integer start_param) {
        llResetScript();
    }
    state_entry() {
        llSetText("", <0, 0, 0>, 1);
        llSleep(1);
        while (llLinksetDataRead("_c") == "") llSleep(0.5);
        xLow = LSDReadIntDefault("xLow", 0);
        yLow = LSDReadIntDefault("yLow", 0);
        zLow = LSDReadIntDefault("zLow", 0);
        xHigh = LSDReadIntDefault("xHigh", 0);
        yHigh = LSDReadIntDefault("xHigh", 0);
        zHigh = LSDReadIntDefault("xHigh", 0);
        every = LSDReadIntDefault("every", 300);
        _LastSniff = llGetUnixTime() - every;
        llSetTimerEvent(1);
        llSetText("READY", <1, 1, 1>, 0);
        llWhisper(0, "Recorder freemem: " + (string)llGetFreeMemory());
    }
    timer() {
        if (llLinksetDataRead("_rec") != "") state Recording;
    }
    state_exit() {
        llSetTimerEvent(0);
    }
}

state Recording {
    on_rez(integer start_param) {
        llResetScript();
    }
    state_entry() {
        llSetTimerEvent(TICK);
        llSetText("ALIVE", <1, 1, 1>, 0);
        TellToucher("Recording turned ON");
    }
    timer() {
        integer nao = llGetUnixTime();
        if ((nao - _LastSniff) >= every) {
            _LastSniff = nao;
            Sniff();
        }
        if (llLinksetDataRead("_rec") == "") state default;
    }
    state_exit() {
        llSetTimerEvent(0);
        TellToucher("Recording turned off");
        llSetText("", <0, 0, 0>, 1);
    }
}
