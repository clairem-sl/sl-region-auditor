// BOS Pony Control System v2 - Groom HUD Main Script
// (c) 2023, Claire Morgenthau

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// ########## STATICS/CONSTS ##########

list EMPL = [];
list pSelfTest = [HTTP_METHOD, "GET", HTTP_CUSTOM_HEADER, "x-token"];

// ########## INTERNALS ##########

string urlMy;
integer lastTouch;
string tokCurr;
integer chanMenu;
integer epoLastTest;
integer epoMenu = 0x7FFFFFFF;
key idSelfTest;

// ########## HELPER FUNCTIONS ##########

string SubStringAfter(string haystack, string needle, integer offset) {
    return llDeleteSubString(haystack, 0, llSubStringIndex(haystack, needle) + offset);
}

string $passLsd;
integer GetAccessLevel(key avatar_id, key obj_id, integer check_gtag) {
    // Any negative verdict == block, and takes precedence. So return immediately with -1 (no access / blocked)
    integer av_verdict = (integer)llLinksetDataReadProtected("acc:" + (string)avatar_id, $passLsd);
    if (av_verdict < 0) return (integer)-1;
    if (obj_id == NULL_KEY) obj_id = llList2Key(llGetAttachedList(avatar_id), 0);
    string grp;
    integer gr_verdict = (integer)llLinksetDataReadProtected("acc:" + (grp = (string)llGetObjectDetails(obj_id, (list)OBJECT_GROUP)), $passLsd);
    if (gr_verdict < 0) return (integer)-1;
    if (check_gtag) {
        string gtag = llStringTrim((string)llGetObjectDetails(avatar_id, (list)OBJECT_GROUP_TAG), STRING_TRIM);
        if ((check_gtag = (integer)llLinksetDataReadProtected("acc:" + grp + ":" + gtag, $passLsd)) < 0) return -1;
        if (check_gtag > gr_verdict) gr_verdict = check_gtag;
    }
    if (av_verdict >= gr_verdict) return av_verdict;
    else return gr_verdict;
}


string $CardName;
key $DataServerRead;
integer $Idx;
StartCard(string card_name) {
    $DataServerRead = llGetNotecardLine(($CardName = card_name), ($Idx = 0));
}


NewToken() {
    tokCurr = llGetSubString((string)llGenerateKey(), 0, 7);
}

// ########## STATES ##########

// WARNING : DO NOT EVER call llLinksetDataReset() in this script !!
//           That is the privilege of main.lsl ONLY !!

default {
    on_rez(integer start_param) {
        llLinksetDataReset();
        llResetScript();
    }
    state_entry() {
        $passLsd = (string)llFrand(65535);
        llLinksetDataReset();
        llLinksetDataWrite("xLow", "0");
        llLinksetDataWrite("xHigh", "256");
        llLinksetDataWrite("yLow", "0");
        llLinksetDataWrite("yHigh", "256");
        llLinksetDataWrite("zLow", "0");
        llLinksetDataWrite("zHigh", "4001");
        StartCard("auditor.conf");
    }
    dataserver(key req, string data) {
        if ($DataServerRead == req) {
            llSetText("Reading config ... " + (string)$Idx, <1, 1, 0>, 1);
            integer is_eof;
            if (!(is_eof = EOF == data)) {
                $DataServerRead = llGetNotecardLine($CardName, ++$Idx);
                data = llStringTrim(data, STRING_TRIM);
                if ("" == data || !llSubStringIndex(data, "#")) return;
            }
            if ("auditor.conf" == $CardName) {
                if (is_eof) {
                    llLinksetDataWrite("_c", "1");
                    state Operational;
                }
                list parts = llParseString2List(data, ["="], EMPL);
                string lsdk = llStringTrim(llList2String(parts, 0), STRING_TRIM );
                string valu = llStringTrim(llList2String(parts, 1), STRING_TRIM );
                string pass = "";
                if ("allow" == lsdk) if ((key)valu) {
                    lsdk = "acc:" + valu;
                    valu = "1";
                    pass = $passLsd;
                }
                llLinksetDataWriteProtected(lsdk, valu, pass);
            }
        }
    }
}


state Operational {
    on_rez(integer start_param) {
        llLinksetDataReset();
        llResetScript();
    }
    state_entry() {
        llSetText("", ZERO_VECTOR, 0);
        lastTouch = 0;
        NewToken();
        llRequestSecureURL();
    }
    http_request(key request_id, string method, string body) {
        if (method == URL_REQUEST_GRANTED ) {
            llWhisper(0, "URL: " + (urlMy = body));
            epoLastTest = llGetUnixTime();
            return;
        }
        if (method == URL_REQUEST_DENIED) {
            request_id = NULL_KEY;
            llWhisper(0, "Cannot get URL. Reason:\n" + body);
            return;
        }
        string ip = llGetHTTPHeader(request_id, "x-remote-ip");
        integer couFail = (integer)llLinksetDataRead("f:" + ip);
        if (couFail >= 3) {
            llHTTPResponse(request_id, 403, "Forbidden: Too many failed attempts.");
            return;
        }
        if (llGetHTTPHeader(request_id, "x-token") != tokCurr) {
            llLinksetDataWrite("f:" + ip, (string)(++couFail));
            llHTTPResponse(request_id, 403, "Forbidden: Wrong token.");
            return;
        }
        if ("GET" == method) {
            string path = llGetSubString(llGetHTTPHeader(request_id, "x-path-info"), 1, -1);
            if ("avis" == path) {
                list found = llLinksetDataFindKeys("^a:", 0, 0);
                integer idx = -llGetListLength(found);
                list resplist;
                if (idx) {
                    do {
                        resplist += llGetSubString(llList2String(found, idx), 2, -1);
                    } while (++idx);
                }
                llHTTPResponse(request_id, 200, llDumpList2String(resplist, ","));
                found = resplist = EMPL;
            }
            else if ((key)path) {
                string resp = llLinksetDataRead("a:" + path);
                if ("" == resp) {
                    llHTTPResponse(request_id, 404, "NotFound: " + path);
                }
                else {
                    llHTTPResponse(request_id, 200, "uuid=" + path + "\n" + resp);
                }
            }
            else if ("ping" == path) {
                llHTTPResponse(request_id, 200, "pong");
            }
            else {
                llHTTPResponse(request_id, 400, "BadRequest: " + path);
            }
        }
        else {
            llHTTPResponse(request_id, 405, method + " not allowed.");
        }
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id != idSelfTest) return;
        if (200 != status || "pong" != body) {
            llReleaseURL(urlMy);
            llRequestSecureURL();            
        }
    }
    touch(integer num_detected) {
        // Menu is still shown
        if (epoMenu != 0x7FFFFFFF) return;
        key toucher = llDetectedKey(0);
        if (GetAccessLevel(toucher, NULL_KEY, FALSE) < 1) {
            llRegionSayTo(toucher, 0, "Not Allowed");
            return;
        }
        llLinksetDataWrite("_t", toucher);
        integer nao = llGetUnixTime();
        if ((nao - lastTouch) < 2) return;
        lastTouch = nao;
        string desc = "RegionAuditor::Agents\n\nRecording: ";
        list menu;
        if (llLinksetDataRead("_rec") != "") {
            desc += "ON\n";
            menu += "Rec ◉";
        }
        else {
            desc += "OFF\n";
            menu += "Rec ◯";
        }
        desc += "Token: " + tokCurr + "\n";
        menu += "NewTok";
        menu += "RESET";
        llListen((chanMenu = (((integer)llFrand(65536)) | 0xC0000000)), "", "", "");
        llDialog(llDetectedKey(0), desc, menu, chanMenu);
        epoMenu = nao;
    }
    changed(integer change) {
        if (change & (CHANGED_INVENTORY | CHANGED_REGION_START)) {
            // CHANGED_INVENTORY    => most likely config notecard is changed
            // CHANGED_REGION_START => region restarted
            llLinksetDataDelete("_on");
            llReleaseURL(urlMy);
            llResetScript();
        }
    }
    listen(integer channel, string name, key id, string message) {
        if (channel == chanMenu) {
            if ("Rec" == llGetSubString(message, 0, 2)) {
                if (llLinksetDataRead("_rec") != "") llLinksetDataDelete("_rec");
                else llLinksetDataWrite("_rec", "1");
            }
            else if ("NewTok" == message) {
                NewToken();
            }
            else if ("RESET" == message) {
                llLinksetDataReset();
                llResetScript();
            }
            llListenRemove(chanMenu);
            epoMenu = 0x7FFFFFFF;
        }
    }
    timer() {
        integer nao = llGetUnixTime();
        if ((nao - epoLastTest) > 300) {
            idSelfTest = llHTTPRequest(urlMy + "/ping", pSelfTest + tokCurr, "");
            epoLastTest = nao;
        }
        if ((nao - epoMenu) > 180) {
            epoMenu = 0x7FFFFFFF;
            llListenRemove(chanMenu);
        }
    }
}
